#Explicando el plot
def start
	puts "+----------------------------------+"
	puts "+            HACKZILLA             +"
	puts "+----------------------------------+"
	puts " "	
	puts  "-------------- Info --------------"
	puts " "
	quote =  "Hackzilla es un legendario y sobrenatural mounstruo que cada 20"
	quote += " años emerge de las profundidades del océano venezolano"
	quote += "para causar estragos y destrozos en la ciudad de Caracas y sus alrededores"
	quote += "y ese es justo el tiempo que ha pasado desde su último"
	quote += "avistamiento en la Región Capital de nuestro país."
	quote += "Es por ello que se está planificando una serie de"
	quote += "actividades militares que permitirán combatir al monstruo"
	quote += "apenas emerja a la superficie marítima y exterminarlo antes de que llegue a la orilla."
	quote += "Se cree que Hackzilla llegará a tierra firme mediante el Balneario de Macuto,"
	quote += "por lo que el armamento será desplegado en esa zona."
	quote += "Tanto la operación militar como la defensa de las ciudades cercanas"
	quote += "al lugar de llegada del monstruo fueron asignadas a un cuerpo de milicias compuesto por civiles"
	quote += "ya que las fuerzas militares oficiales se encuentran ocupadas en otras actividades (cobardes) y no están disponibles para combatir al monstruo."
	quote += "El miembro más astuto (o sobrio) de dicho cuerpo de milicias será el encargado"
	quote += "de planificar la estrategia defensiva contra el monstruo, y para esto cuenta con algunos explosivos de gran poder."
	puts quote
	puts " "
	puts "Tu eres el Sargento Sobrio, seras capaz de defender la costa contra las fuerzas del imperialismo oceanico?"
	puts "."
	puts "."
	puts "."
end
#Simplemente pregunta Si o No y se asegura que solo sean posibles esas respuestas
def interfaz_usuario(opc)
	loop do 
		if opc!="y" && opc!="Y" && opc!="n" && opc!="N" then
			puts "Comando incorrecto, ingresar: y o n"
			opc=gets.chomp
		end
		break if (opc=="y" or opc=="Y" or opc=="n" or opc=="N")
	end
	return opc
end
#Cambia las dimensiones del tablero
def opc_tablero(size)
	puts "Deseas cambiar la dimension del tablero? Y/N"
		opc=gets.chomp
		n=interfaz_usuario(opc)
		if n=="y" or n=="Y" then
			puts "Cual sera el valor del ancho?"
			a=gets.chomp.to_i
			puts "Cual sera el valor del alto?"
			l=gets.chomp.to_i
			return [l,a]
		else
			size=[15,10]
			return size
			puts "El tablero sera entonces de 10x15"
		end
end
#Menu principal
def main(size,mines,torpedos)
	puts "opciones? Y/N"
	opc=gets.chomp
	n=interfaz_usuario(opc)	
	if n=="y" or n=="Y" then
		system("clear")
		puts "La configuraion por default es un tablero de 10x15"
		puts "15 minas con un daño de 35 cada una"
		puts "3 torpedos con un daño de 50 cada uno"
		puts "Desea modificar estas opciones?"
		opc=gets.chomp
		n=interfaz_usuario(opc)
		if n=="y" or n=="Y" then
			system("clear")		
			puts "t:tablero m:minas r:torpedos"
			opc=gets.chomp
			if opc=="t" then
				system("clear")
				size=opc_tablero(size)
			elsif opc=="m" then
				system("clear")
				opc_minas
			elsif opc=="r" then
				system("clear")
				opc_torpedos
			else
				puts "Opcion incorrecta, elija t/ m/ r/"
				opciones
			end
		elsif n=="n" or n=="N" then
			system("clear")
			main
	end
	else
		puts "Deseas empezar un juego nuevo? Y/N"
		opc=gets.chomp
		n=interfaz_usuario(opc)
	return n
	end
end
#Tablero default
def tablero(size,t)
	system("clear")
	s=size
#	t=[]
	for i in 0..s[0] do
		t[i]=[]
		for j in 0..s[1]-1 do
		t[i][j]= "࿔"
		end
	end
	for i in 0..s[1]-1 do
		t[(s[0])][i]= "༜"
	end
	return t
#	for i in 0..s[0] do
#		puts "#{t[i].to_s.gsub('"', '')}"
#	end
end
#Rehace el tablero cada vez que se necesite
#Revisa donde esta el cursor
def cursor_pos(t)
	for i in 0..t.length-1 do
		for j in 0..t[0].length do
			if t[i][j]=="⊡" then
				n=[i,j]
				return n
			end
		end
	end
end
def print_tablero(t)
	for i in 0..t.length-1 do
		puts "#{t[i].to_s.gsub('"', '')}"
	end
end
#Cursor de movimiento para poner las minas
def get_keypressed_mines(t,mines,mine_token)
	t[15][0]="⊡"
	print_tablero(t)
	aux="࿔"
	puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
	loop do
		system("stty raw -echo")
		n = STDIN.getc.downcase
		system("stty -raw echo")
			case n.ord
				when 119 then
					pos=cursor_pos(t)
					if pos[0]==0 then
						system("clear")						
						t[(pos[0])][(pos[1])]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "					
					elsif pos[0]==15 && t[(pos[0]-1)][(pos[1])]!=mine_token then
						system("clear")
						t[(pos[0]-1)][(pos[1])]="⊡"
						t[(pos[0])][(pos[1])]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif pos[0]==15 && t[(pos[0]-1)][(pos[1])]==mine_token then
						system("clear")						
						aux=mine_token
						t[(pos[0]-1)][(pos[1])]="⊡"
						t[(pos[0])][(pos[1])]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif pos[0]<15 && t[(pos[0]-1)][(pos[1])]==mine_token && aux!=mine_token then
						system("clear")						
						aux=mine_token
						t[(pos[0]-1)][(pos[1])]="⊡"
						t[(pos[0])][(pos[1])]="࿔"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif pos[0]<15 && t[(pos[0]-1)][(pos[1])]==mine_token && aux==mine_token then
						system("clear")						
						t[(pos[0])][(pos[1])]=aux
						aux=mine_token
						t[(pos[0]-1)][(pos[1])]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[(pos[0]-1)][(pos[1])]!=mine_token then
						system("clear")						
						aux=t[(pos[0]-1)][(pos[1])]
						t[(pos[0])][(pos[1])]=mine_token
						t[(pos[0]-1)][(pos[1])]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					else
						system("clear")						
						t[(pos[0]-1)][(pos[1])]="⊡"
						t[(pos[0])][(pos[1])]="࿔"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					end
				when 115 then
					pos=cursor_pos(t)
					if pos[0]==15 then
						system("clear")						
						t[(pos[0])][pos[1]]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[pos[0]+1][pos[1]]!=mine_token then
						system("clear")				
						aux=t[pos[0]+1][pos[1]]
						t[(pos[0])][pos[1]]=mine_token
						t[(pos[0]+1)][(pos[1])]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux!=mine_token && t[pos[0]+1][pos[1]]==mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=aux
						aux=mine_token
						t[(pos[0]+1)][(pos[1])]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[pos[0]+1][pos[1]]==mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=aux
						aux=mine_token
						t[(pos[0]+1)][(pos[1])]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					else
						system("clear")
						t[(pos[0]+1)][(pos[1])]="⊡"
						t[(pos[0])][pos[1]]="࿔"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					end
				when 100 then
					pos=cursor_pos(t)
					if pos[1]==9 then
						system("clear")
						t[(pos[0])][pos[1]]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif pos[0]==15 then
						system("clear")
						t[(pos[0])][(pos[1]+1)]="⊡"
						t[(pos[0])][pos[1]]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[pos[0]][pos[1]+1]!=mine_token then
						system("clear")			
						aux=t[pos[0]][pos[1]+1]
						t[(pos[0])][pos[1]]=mine_token
						t[(pos[0])][(pos[1])+1]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux!=mine_token && t[pos[0]][pos[1]+1]==mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=aux
						aux=mine_token
						t[(pos[0])][(pos[1])+1]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[pos[0]][pos[1]+1]==mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=aux
						aux=mine_token
						t[(pos[0])][(pos[1])+1]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					else
						system("clear")
						t[(pos[0])][(pos[1]+1)]="⊡"
						t[(pos[0])][pos[1]]="࿔"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					end
				when 97 then
					pos=cursor_pos(t)
					if pos[1]==0 then
						system("clear")
						t[(pos[0])][pos[1]]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif pos[0]==15 then
						system("clear")
						t[(pos[0])][(pos[1]-1)]="⊡"
						t[(pos[0])][pos[1]]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[pos[0]][pos[1]-1]!=mine_token then
						system("clear")			
						aux=t[pos[0]][pos[1]-1]
						t[(pos[0])][pos[1]]=mine_token
						t[(pos[0])][(pos[1])-1]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux!=mine_token && t[pos[0]][pos[1]-1]==mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=aux
						aux=mine_token
						t[(pos[0])][(pos[1])-1]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					elsif aux==mine_token && t[pos[0]][pos[1]-1]==mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=aux
						aux=mine_token
						t[(pos[0])][(pos[1])-1]="⊡"
						print_tablero(t) 
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					else
						system("clear")
						t[(pos[0])][(pos[1]-1)]="⊡"
						t[(pos[0])][pos[1]]="࿔"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
					end
				when 32 then
					pos=cursor_pos(t)
					if aux==mine_token then
						system("clear")
						print_tablero(t)
						puts "Ya hay una mina en esa posicion"
					elsif pos[0]<14 && pos[0]>0 && aux!=mine_token then
						system("clear")
						t[(pos[0])][pos[1]]=mine_token
						t[15][0]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar mina ESPACIO, salir m: "
						mines-=1
					elsif pos[0]==0 then
						system("clear")
						t[15][0]="⊡"
						t[(pos[0])][pos[1]]="࿔"
						print_tablero(t)
						puts "Demasiado lejos, ya esas no son aguas venezolanas"
					elsif pos[0]==14 then
						system("clear")
						t[15][0]="⊡"
						t[(pos[0])][pos[1]]="࿔"
						print_tablero(t)
						puts "Ahi van los torpedos de protones, para que no te toquen los cojones"
					else
						system("clear")
						t[15][0]="⊡"
						t[(pos[0])][pos[1]]="༜"
						print_tablero(t)
						puts "No se pueden colocar minas marinas en la tierra, deja el cocuy"
					end
				when 109 then
					pos=cursor_pos(t)
					if pos[0]<15 then
						t[(pos[0])][pos[1]]=aux
						t[15][0]="⊡"
						mines=0	
					else
						t[(pos[0])][pos[1]]="༜"
						t[15][0]="⊡"
						mines=0
					end
			end
			break if (n == "m" or mines==0)
	end
	return mines
end
#Funcion para poner las minas
def put_mines(t)
	mines=15
	mine_token="۞"
	while mines>0 do
		mines=get_keypressed_mines(t,mines,mine_token)
	end
	t[15][0]="༜"
	system("clear")
	print_tablero(t)
end
#Busca donde esta Hackzilla
def hackzilla_pos(t)
	for i in 0..t.length-1 do
		for j in 0..t[0].length do
			if t[i][j]=="Ψ" then
				n=[i,j]
				return n
			end
		end
	end
end
#Hackzilla se mueve hacia abajo
def mov_hackzilla_down(t,aux)
	pos=hackzilla_pos(t)
		if pos[0]==0 && t[pos[0]+1][pos[1]]!="۞" then				
			aux=t[pos[0]+1][pos[1]]
			t[pos[0]+1][pos[1]]="Ψ"
			t[pos[0]][pos[1]]=aux
			print_tablero(t)
			return aux
		elsif pos[0]==0 && t[pos[0]+1][pos[1]]=="۞" then				
			aux=t[pos[0]+1][pos[1]]
			t[pos[0]+1][pos[1]]="Ψ"
			t[pos[0]][pos[1]]="࿔"
			print_tablero(t)
			return aux
		elsif aux=="۞" then				
			aux=t[pos[0]+1][pos[1]]
			t[pos[0]+1][pos[1]]="Ψ"
			t[pos[0]][pos[1]]="X"
			print_tablero(t)
			return aux
		elsif aux=="࿔" then				
			aux=t[pos[0]+1][pos[1]]
			t[pos[0]+1][pos[1]]="Ψ"
			t[pos[0]][pos[1]]="࿔"
			print_tablero(t)
			return aux
		elsif aux=="X" then				
			aux=t[pos[0]+1][pos[1]]
			t[pos[0]+1][pos[1]]="Ψ"
			t[pos[0]][pos[1]]="X"
			print_tablero(t)
			return aux
		else
		end
end
#Hackzilla se mueve a la derecha
def mov_hackzilla_right(t,aux)
	pos=hackzilla_pos(t)
		if pos[0]==0 then					
			aux=t[pos[0]][pos[1]+1]
			t[pos[0]][pos[1]+1]="Ψ"
			t[pos[0]][pos[1]]=aux
			print_tablero(t)
			return aux
		elsif pos[0]>0 && aux=="۞" then				
			aux=t[pos[0]][pos[1]+1]
			t[pos[0]][pos[1]+1]="Ψ"
			t[pos[0]][pos[1]]="X"
			print_tablero(t)
			return aux
		elsif pos[0]>0 && aux=="࿔" then				
			aux=t[pos[0]][pos[1]+1]
			t[pos[0]][pos[1]+1]="Ψ"
			t[pos[0]][pos[1]]="࿔"
			print_tablero(t)
			return aux
		elsif pos[0]>0 && aux=="X" then			
			aux=t[pos[0]][pos[1]+1]
			t[pos[0]][pos[1]+1]="Ψ"
			t[pos[0]][pos[1]]="X"
			print_tablero(t)
			return aux
		else
		end
end
#Hackzilla se mueve hacia la izquierda
def mov_hackzilla_left(t,aux)
	pos=hackzilla_pos(t)
		if pos[0]==0 then				
			aux=t[pos[0]][pos[1]-1]
			t[pos[0]][pos[1]-1]="Ψ"
			t[pos[0]][pos[1]]=aux
			print_tablero(t)
			return aux
		elsif pos[0]>0 && aux=="۞" then				
			aux=t[pos[0]][pos[1]-1]
			t[pos[0]][pos[1]-1]="Ψ"
			t[pos[0]][pos[1]]="X"
			print_tablero(t)
			return aux
		elsif pos[0]>0 && aux=="࿔" then				
			aux=t[pos[0]][pos[1]-1]
			t[pos[0]][pos[1]-1]="Ψ"
			t[pos[0]][pos[1]]="࿔"
			print_tablero(t)
			return aux
		elsif pos[0]>0 && aux=="X" then				
			aux=t[pos[0]][pos[1]-1]
			t[pos[0]][pos[1]-1]="Ψ"
			t[pos[0]][pos[1]]="X"
			print_tablero(t)
			return aux
		else
		end
end
#Hackzilla se recupera
def hackzilla_recover(hackzilla)
	if hackzilla<95 then
		hackzilla+=10
	elsif hackzilla==95 then
		hackzilla=100
	end
	return hackzilla
end
#Movimiento de Hackzilla
def mov_hackzilla(t,hackzilla)
	n=rand
	aux="࿔"
	pos=hackzilla_pos(t)
	if hackzilla>30 then
		if n<0.25 then
			aux=mov_hackzilla_down(t,aux)
			return aux
		elsif n>=0.25 && n<0.50 then
			pos=hackzilla_pos(t)
			if pos[1]==9 then
				n=rand 
				if n<=0.5 then
					aux=mov_hackzilla_down(t,aux)
					return aux
				else
					aux=mov_hackzilla_left(t,aux)
					return aux
				end
			else
				aux=mov_hackzilla_right(t,aux)
				return aux
			end
			elsif n>=0.50 && n<0.75 then	
				if pos[1]==0 then
					n=rand 
					if n<=0.5 then
						aux=mov_hackzilla_down(t,aux)
						return aux
					else
						aux=mov_hackzilla_right(t,aux)
						return aux
					end
				else
					aux=mov_hackzilla_left(t,aux)
					return aux
				end
			elsif n>=0.75 then
				hackzilla=hackzilla_recover(hackzilla)
				system("clear")
				t[15][0]="༜"
				print_tablero(t)
				return hackzilla
			else
			end
	elsif hackzilla<=30 then
		if n<0.1 then
			aux=mov_hackzilla_down(t,aux)
			return aux
		elsif n>=0.1 && n<0.2 then
			pos=hackzilla_pos(t)
			if pos[1]==9 then
				n=rand
				if n<=0.5 then
					aux=mov_hackzilla_down(t,aux)
					return aux
				else
					aux=mov_hackzilla_left(t,aux)
					return aux
				end
			else
			aux=mov_hackzilla_right(t,aux)
			return aux
			end
		elsif n>=0.2 && n<0.3 then	
			if pos[1]==0 then
				n=rand 
				if n<=0.5 then
					aux=mov_hackzilla_down(t,aux)
					return aux
				else
					aux=mov_hackzilla_right(t,aux)
					return aux
				end
			else
				aux=mov_hackzilla_left(t,aux)
				return aux
			end
		elsif n>=0.3 then
			hackzilla=hackzilla_recover(hackzilla)
			system("clear")
			puts "Hackzilla HP:#{hackzilla}"
			t[15][0]="༜"
			print_tablero(t)
			return hackzilla
		else
		end
	else
	end
end
#Explosion de Mina
def check_hp(chk)
	if chk=="۞" then
		return 35.to_i
	else
		return 0.to_i
	end
end
def game_chk(t,hackzilla)
	pos=hackzilla_pos(t)	
	if hackzilla<=0 then
		puts "Ganaste! Chiguere pa' todo el mundo en el CLAP"
		return true
	elsif pos[0]==15 then
		puts "Hackzilla llego a Macuto! Es culpa del imperialismo!"
		return true
	else
	end
end
#Cursor de torpedos
def get_keypressed_torpedos(t,torpedos,torpedo_token)
	t[15][0]="⊡"
	print_tablero(t)
	puts "Movimiento con WASD, colocar torpedo ESPACIO, salir m: "
	loop do
		system("stty raw -echo")
		n = STDIN.getc.downcase
		system("stty -raw echo")
			case n.ord
				when 100 then
					pos=cursor_pos(t)
					if pos[1]==9 then
						system("clear")
						t[(pos[0])][pos[1]]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar torpedo ESPACIO, salir m: "
					else 
						system("clear")
						t[(pos[0])][(pos[1])+1]="⊡"
						t[(pos[0])][(pos[1])]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar torpedo ESPACIO, salir m: "
					end
				when 97 then
					pos=cursor_pos(t)
					if pos[1]==0 then
						system("clear")
						t[(pos[0])][pos[1]]="⊡"
						print_tablero(t)
						puts "Movimiento con WASD, colocar torpedo ESPACIO, salir m: "
					else 
						system("clear")
						t[(pos[0])][(pos[1])-1]="⊡"
						t[(pos[0])][(pos[1])]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar torpedo ESPACIO, salir m: "
					end
				when 32 then
					pos=cursor_pos(t)
					if t[pos[0]-1][pos[1]]!="࿔" && t[pos[0]-1][pos[1]]!="Ψ" then
						system("clear")
						print_tablero(t)
						puts "Ya hay un torpedo en esa posicion"
					elsif t[pos[0]-1][pos[1]]=="Ψ" then
						system("clear")
						print_tablero(t)
						puts "Hackzilla esta demasiado cerca!"
					else
						system("clear")
						t[(pos[0])-1][pos[1]]=torpedo_token
						t[15][0]="⊡"
						t[(pos[0])][pos[1]]="༜"
						print_tablero(t)
						puts "Movimiento con WASD, colocar torpedo ESPACIO, salir m: "
						torpedos-=1
					end
			end
			break if (n == " " or torpedos==0)
	end
	return torpedos
end
#Disparo de torpedos
def disparar_torpedo(t,torpedos,torpedo_token)
	system("clear")	
	tor=get_keypressed_torpedos(t,torpedos,torpedo_token)
	return tor
end
#Turno del jugador
def mov_player(t,torpedos,torpedo_token)
	puts "Deseas disparar un torpedo? Y/N"
	opc=gets.chomp
	n=interfaz_usuario(opc)
	if torpedos==3 then
		torpedo_token="۩"
	elsif torpedos==2 then
		torpedo_token="Δ"
	elsif torpedos==1
		torpedo_token="٨"
	else
	end
	if n=="y" or n=="Y" then
		tor=disparar_torpedo(t,torpedos,torpedo_token)
		if tor>0 then		
			puts "Deseas disparar otro torpedo? Y/N"
			opc=gets.chomp
			n=interfaz_usuario(opc)
			if n=="y" or n=="Y" then
				torpedos=tor
				if tor==2 then
					torpedo_token="Δ"
				elsif tor==1
					torpedo_token="٨"
				else
				end
				tor=disparar_torpedo(t,torpedos,torpedo_token)
				if tor>0 then		
					puts "Deseas disparar otro torpedo? Y/N"
					opc=gets.chomp
					n=interfaz_usuario(opc)
					if n=="y" or n=="Y" then
						torpedo_token="٨"
						tor=disparar_torpedo(t,torpedos,torpedo_token)
					else
						puts "Quedan #{tor} torpedos"
					end
				else
					puts "No quedan mas torpedos presiona cualquier tecla para avanzar el turno"
					o=gets.chomp
				end
			else
				puts "Quedan #{tor} torpedos"
			end
		else
			puts "No quedan mas torpedos presiona cualquier tecla para avanzar el turno"
			o=gets.chomp
		end
	elsif n=="n" or n=="N" then
		puts "No se disparo ningun torpedo, presiona cualquier tecla para avanzar el turno"
		o=gets.chomp
	end
	return tor
end
#Busca donde esta el Torpedo 1
def torpedo1_pos(t)
	for i in 0..t.length-1 do
		for j in 0..t[0].length do
			if t[i][j]=="۩" then
				n=[i,j]
				return n
			end
		end
	end
end
#Mueve al torpedo 1
def mov_torpedo1(t,aux1)
	pos=torpedo1_pos(t)
	if t[pos[0]-1][pos[1]]=="࿔" && aux1=="࿔" then		
		aux1="࿔"
		t[pos[0]-1][pos[1]]="۩"
		t[pos[0]][pos[1]]=aux1
		return aux1
	elsif t[pos[0]-1][pos[1]]=="࿔" && aux1=="۞" then		
		aux1="࿔"
		t[pos[0]-1][pos[1]]="۩"
		t[pos[0]][pos[1]]="۞"
		return aux1
	elsif t[pos[0]-1][pos[1]]=="۞" && aux1=="࿔" then	
		aux1="۞"
		t[pos[0]-1][pos[1]]="۩"
		t[pos[0]][pos[1]]=aux1
		return aux1
	elsif t[pos[0]-1][pos[1]]=="۞" && aux1=="۞" then	
		aux1="۞"
		t[pos[0]-1][pos[1]]="۩"
		t[pos[0]][pos[1]]=aux1
		return aux1
	elsif t[pos[0]-1][pos[1]]=="Ψ" && aux1=="۞" then	
		aux1="Ψ"
		t[pos[0]][pos[1]]="۞"
		t[0][0]="۩"
		return aux1
	elsif t[pos[0]-1][pos[1]]=="Ψ" && aux1=="࿔" then	
		aux1="Ψ"
		t[pos[0]][pos[1]]="࿔"
		t[0][0]="۩"
		return aux1
	elsif pos[0]==0 then		
		aux="࿔"
		t[pos[0]][pos[1]]="۩"
		return aux1
	else 
	end
end
#Busca donde esta el Torpedo 2
def torpedo2_pos(t)
	for i in 0..t.length-1 do
		for j in 0..t[0].length do
			if t[i][j]=="Δ" then
				n=[i,j]
				return n
			end
		end
	end
end
#Mueve al torpedo 2
def mov_torpedo2(t,aux2)
	pos=torpedo2_pos(t)
	if t[pos[0]-1][pos[1]]=="࿔" && aux2=="࿔" then	
		aux2="࿔"
		t[pos[0]-1][pos[1]]="Δ"
		t[pos[0]][pos[1]]=aux2
		return aux2
	elsif t[pos[0]-1][pos[1]]=="࿔" && aux2=="۞" then		
		aux2="࿔"
		t[pos[0]-1][pos[1]]="Δ"
		t[pos[0]][pos[1]]="۞"
		return aux2
	elsif t[pos[0]-1][pos[1]]=="۞" && aux2=="࿔" then	
		aux2="۞"
		t[pos[0]-1][pos[1]]="Δ"
		t[pos[0]][pos[1]]=aux2
		return aux2
	elsif t[pos[0]-1][pos[1]]=="۞" && aux2=="۞" then	
		aux2="۞"
		t[pos[0]-1][pos[1]]="Δ"
		t[pos[0]][pos[1]]=aux
		return aux2
	elsif t[pos[0]-1][pos[1]]=="Ψ" && aux2=="۞" then	
		aux2="Ψ"
		t[pos[0]][pos[1]]="۞"
		t[0][1]="Δ"
		return aux2
	elsif t[pos[0]-1][pos[1]]=="Ψ" && aux2=="࿔" then	
		aux2="Ψ"
		t[pos[0]][pos[1]]="࿔"
		t[0][1]="Δ"
		return aux2
	elsif pos[0]==0 then	
		aux="࿔"
		t[pos[0]][pos[1]]="Δ"
		return aux2
	else
	end
end
#Busca donde esta el Torpedo 3
def torpedo3_pos(t)
	for i in 0..t.length-1 do
		for j in 0..t[0].length do
			if t[i][j]=="٨" then
				n=[i,j]
				return n
			end
		end
	end
end
#Mueve al torpedo 3
def mov_torpedo3(t,aux3)
	pos=torpedo3_pos(t)
	aux="࿔"
	if t[pos[0]-1][pos[1]]=="࿔" && aux3=="࿔" then
		t[pos[0]-1][pos[1]]="٨"
		t[pos[0]][pos[1]]=aux3
		return aux3
	elsif t[pos[0]-1][pos[1]]=="࿔" && aux3=="۞" then	
		aux3="࿔"
		t[pos[0]-1][pos[1]]="٨"
		t[pos[0]][pos[1]]="۞"
		return aux3
	elsif t[pos[0]-1][pos[1]]=="۞" && aux3=="࿔" then	
		aux3="۞"
		t[pos[0]-1][pos[1]]="٨"
		t[pos[0]][pos[1]]=aux3
		return aux3
	elsif t[pos[0]-1][pos[1]]=="۞" && aux3=="۞" then	
		aux3="۞"
		t[pos[0]-1][pos[1]]="٨"
		t[pos[0]][pos[1]]=aux3
		return aux3
	elsif t[pos[0]-1][pos[1]]=="Ψ" && aux3=="۞" then	
		aux3="Ψ"
		t[pos[0]][pos[1]]="۞"
		t[0][2]="٨"
		return aux3
	elsif t[pos[0]-1][pos[1]]=="Ψ" && aux3=="࿔" then	
		aux3="Ψ"
		t[pos[0]][pos[1]]="࿔"
		t[0][1]="٨"
		return aux3
	elsif pos[0]==0 then	
		aux3="࿔"
		t[pos[0]][pos[1]]="٨"
		return aux3
	else
	end
end
#Explosion del torpedo
def chk_torpedo(t,aux1,aux2,aux3,hackzilla)
	system("clear")
	dmg=50
	puts "Hackzilla HP:#{hackzilla}"
	if aux1=="Ψ" then
		hackzilla-=dmg
		puts "Hackzilla HP:#{hackzilla}"
		print_tablero(t)
		return hackzilla
	elsif aux2=="Ψ" then
		hackzilla-=dmg
		puts "Hackzilla HP:#{hackzilla}"
		print_tablero(t)
		return hackzilla
	elsif aux3=="Ψ" then
		hackzilla-=dmg
		puts "Hackzilla HP:#{hackzilla}"
		print_tablero(t)
		return hackzilla
	else
		puts "Hackzilla HP:#{hackzilla}"
		print_tablero(t)
		return hackzilla
	end
end	
#Empieza el juego
def new_game(t,turno,torpedos,minas,torpedo_token)
	hackzilla=100
	put_mines(t)
	t[0][(rand(t[0].length))]="Ψ"
	turn=turno
	print_tablero(t)
	game_end=false
	while game_end!=true do
		if turn%2==0 then		
			system("clear")
			while torpedos!=0 do	
				roq=mov_player(t,torpedos,torpedo_token)
				torpedos=roq
			end
			puts "Hackzilla HP:#{hackzilla}"
			print_tablero(t)
			puts "Presiona cualquier tecla para avanzar tu el turno"
			o=gets.chomp
			turn+=1
		elsif turn%2!=0 then
			pos1=hackzilla_pos(t)			
			chk=mov_hackzilla(t,hackzilla)
			pos2=hackzilla_pos(t)
			if pos1[0]==pos2[0] && pos1[1]==pos2[1] then
				hackzilla=chk
			else
				hp=check_hp(chk)
				hackzilla-=hp
			end
			aux1="࿔"
			aux2="࿔"
			aux3="࿔"
			aux1=mov_torpedo1(t,aux1)
			aux2=mov_torpedo2(t,aux2)
			aux3=mov_torpedo3(t,aux3)
			dmg=chk_torpedo(t,aux1,aux2,aux3,hackzilla)
			hackzilla=dmg
			puts "Presiona cualquier tecla para avanzar el turno de Hackzilla"
			o=gets.chomp
			turn+=1
			game_end=game_chk(t,hackzilla)
		else		
		end
	end
end
#Proceso principal de todo el juego
def game
	system("clear")
	start
	size=[15,10]
	mines=[15,35]
	torpedos=[3,50]
	t=[]
	n=main(size,mines,torpedos)
	if n=="y" or n=="Y" then
		system("clear")	
		tablero(size,t)
		turno=0
		torpedos=3
		minas=15
		torpedo_token="۩"
		new_game(t,turno,torpedos,minas,torpedo_token)
	elsif n=="n" or n=="N" then
		system("clear")
		puts "Adios"
	end
end
game
